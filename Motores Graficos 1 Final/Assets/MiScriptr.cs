using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiScriptr : MonoBehaviour
{
    private Animator animator;

    void Start()
    {
        // Obt�n la referencia al componente Animator
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        // Activa la animaci�n cuando se presiona una tecla
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Activa la animaci�n llamando a un trigger
            animator.SetTrigger("MiTrigger");
        }
    }
}
