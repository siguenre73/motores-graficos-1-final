using UnityEngine;

public class SwordAttack : MonoBehaviour
{
    public LayerMask enemyLayer;

    private void OnTriggerEnter(Collider other)
    {
        if (enemyLayer == (enemyLayer | (1 << other.gameObject.layer)))
        {
            Destroy(other.gameObject);
        }
    }
}
