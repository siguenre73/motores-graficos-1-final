using UnityEngine;

public class EnemyAttraction : MonoBehaviour
{
    public float attractionSpeed = 5f;
    public float throwForce = 10f;
    private GameObject enemyToAttract;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            // Buscar el enemigo m�s cercano y establecerlo como objetivo de atracci�n
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, 10f, LayerMask.GetMask("Enemy"));
            if (hitColliders.Length > 0)
            {
                enemyToAttract = hitColliders[0].gameObject;
            }
        }

        if (Input.GetKeyDown(KeyCode.F) && enemyToAttract != null)
        {
            // Lanzar al enemigo
            Rigidbody enemyRigidbody = enemyToAttract.GetComponent<Rigidbody>();
            Vector3 throwDirection = (enemyToAttract.transform.position - transform.position).normalized;
            enemyRigidbody.velocity = throwDirection * throwForce;

            // Reiniciar la referencia al enemigo atrayendo
            enemyToAttract = null;
        }

        if (enemyToAttract != null)
        {
            // Mover al enemigo hacia el jugador
            enemyToAttract.transform.position = Vector3.MoveTowards(enemyToAttract.transform.position, transform.position, attractionSpeed * Time.deltaTime);
        }
    }
}
