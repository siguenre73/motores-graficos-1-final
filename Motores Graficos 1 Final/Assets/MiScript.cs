// Ejemplo de c�mo activar una animaci�n desde un script
using UnityEngine;

public class MiScript : MonoBehaviour
{
    private Animator animator;

    void Start()
    {
        // Obt�n la referencia al componente Animator
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        // Activa la animaci�n cuando se presiona una tecla
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Activa la animaci�n llamando a un trigger
            animator.SetTrigger("MiTrigger");
        }
    }
}

