using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float dashSpeed = 10f;
    public float dashTime = 0.5f;
    public float dashCooldown = 2f;

    private float dashTimeLeft;
    private float lastDashTime;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift) && Time.time > lastDashTime + dashCooldown)
        {
            StartCoroutine(Dash());
            lastDashTime = Time.time;
        }
    }

    IEnumerator Dash()
    {
        dashTimeLeft = dashTime;

        while (dashTimeLeft > 0)
        {
            // L�gica del dash
            transform.Translate(Vector3.forward * dashSpeed * Time.deltaTime);
            dashTimeLeft -= Time.deltaTime;
            yield return null;
        }
    }
}
