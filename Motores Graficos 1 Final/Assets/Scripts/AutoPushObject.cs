using UnityEngine;

public class AutoPushObject : MonoBehaviour
{
    private Animator animator;
    public float pushForce = 10f;

    void Start()
    {
        // Obt�n la referencia al Animator
        animator = GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto con el que colisiona tiene un Rigidbody
        Rigidbody rb = other.GetComponent<Rigidbody>();

        if (rb != null)
        {
            // Activa la animaci�n de empuje
            animator.SetBool("Push", true);

            // Aplica una fuerza para empujar el objeto
            rb.AddForce(transform.forward * pushForce, ForceMode.Impulse);
        }
    }

    void OnTriggerExit(Collider other)
    {
        // Desactiva la animaci�n de empuje al salir de la colisi�n
        animator.SetBool("Push", false);
    }
}

