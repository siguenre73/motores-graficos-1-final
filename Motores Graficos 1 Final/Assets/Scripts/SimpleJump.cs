using UnityEngine;

public class SimpleJump : MonoBehaviour
{
    public float jumpForce = 5f;
    private Rigidbody rb;

    void Start()
    {
        // Obt�n referencia al Rigidbody
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // L�gica para saltar al presionar la barra espaciadora
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
    }

    void Jump()
    {
        // Aplica una fuerza hacia arriba al Rigidbody para simular el salto
        if (rb != null)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }
}


