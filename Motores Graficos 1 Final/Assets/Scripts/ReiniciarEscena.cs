using UnityEngine;
using UnityEngine.SceneManagement;

public class ReiniciarEscena : MonoBehaviour
{
    private void Update()
    {
        // Reiniciar la escena cuando se presiona la tecla "R"
        if (Input.GetKeyDown(KeyCode.R))
        {
            Reiniciar();
        }
    }

    void Reiniciar()
    {
        // Obtener el �ndice de la escena actual
        int indiceEscenaActual = SceneManager.GetActiveScene().buildIndex;

        // Cargar la escena actual para reiniciarla
        SceneManager.LoadScene(indiceEscenaActual);
    }
}

