using UnityEngine;

public class ControlPersonaje : MonoBehaviour
{
    public float velocidadMovimiento = 5f;
    public float sensibilidadMouse = 2f;

    private void Update()
    {
        // Ocultar el cursor al hacer clic en la pantalla
        if (Input.GetMouseButtonDown(0))
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        // Mover el personaje
        MoverPersonaje();

        // Rotar la c�mara con el mouse
        RotarConMouse();
    }

    void MoverPersonaje()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");

        Vector3 movimiento = new Vector3(movimientoHorizontal, 0f, movimientoVertical) * velocidadMovimiento * Time.deltaTime;
        transform.Translate(movimiento, Space.Self);
    }

    void RotarConMouse()
    {
        float rotacionMouseX = Input.GetAxis("Mouse X") * sensibilidadMouse;

        // Rotar el personaje en el eje vertical (Y)
        transform.Rotate(Vector3.up * rotacionMouseX);
    }
}


